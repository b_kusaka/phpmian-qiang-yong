<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");

//初期化関数を呼び出す
init();

// 正しく入力されていないと確認画面へは遷移してくることができない
if (error_check($_SESSION)) {
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/input.php";
	header("Location: " . $url);
	exit;
}

//下記に２つのボタンのアクションを追加します
	
// 修正ボタンをクリックされたときの処理
if (isset($_POST["modify"])) {
	// 入力フォームへ遷移する
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/input.php";
	header("Location: " . $url);
	exit;
}

// 登録ボタンをクリックされたときの処理
if (isset($_POST["register"])) {
	// 登録ボタン押下後は、第7章で行います。
	print "第7章で処理を解説";
	exit;
}

// Smartyを生成する
$smarty = new MySmarty();

// 性別リストと年齢リストと動物のリストの配列
$smarty->assign("sex_value",   getSexList());
$smarty->assign("age_value",   getAgeList());
$smarty->assign("animal_value",getAnimalList());

$smarty->display("confirm.tpl");

?>
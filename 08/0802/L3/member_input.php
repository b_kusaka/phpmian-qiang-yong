<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");
	
//初期化を行なう
init();

//エラーメッセージや登録メッセージを保持する
$mes = array();

//登録ボタンをクリックされたときの処理
if (isset($_POST["regist"])) {
	//ここにエラーチェックとデータを登録する処理を追記します

}

//Smartyを生成する
$smarty = new MySmarty();

//メッセージを送る
$smarty->assign("mes", $mes);

//ページを表示する
$smarty->display("member_input.tpl");
?>
<?php
// ユーザー定義関数
function user_sum($a,$b) { // 値渡しの引数
	$a = $a + $b; // 引数$aと$bの合計を求めます
 print "合計は" . $a . "です<br>";//合計を表示します
}
?>
<html>
<head>
<title>値渡しのユーザ定義関数</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php
// ユーザー定義関数：引数の値渡し例
$a = 15;
$b = 30;

// ユーザー定義関数の呼び出し
user_sum($a,$b);

// 変数$aと$bの値の表示
print "変数の値は" . $a . " と " . $b . "です<br>";
?>
</body>
</html>

/* HTMLで表示する部分 */
print <<< EOF
<html>
<head>
<title></title>
</head>
<body>
<h2>ボーリング自動得点計算</h2>
<h4>自動で１０フレームを行い、その得点を割り出すプログラムです。</h4>
<table border="1">
EOF;
//フレーム数を表示する（10フレーム
print "<tr>";
for ($i=1;$i<=10;$i++) {
	$col = ($i==10) ? 3 : 2;
	print "<td colspan=\"{$col}\">{$i}Frame</td>";
}
print "</tr>";

//フレームの投球ごとの得点を表示する（10フレーム * 3投
print "<tr>";
for ($i=1;$i<=10;$i++) {
	//最初のピン数
	$pin=10;
	for ($j=1;$j<=3;$j++) {
		if ($i!=10 && $j==3) {break;}

		//値が空の場合は次の値へ
		if (!isset($result[$i][$j])) {continue;}
		
		//ピンの数から倒したピン数を引く
		$pin = $pin - $result[$i][$j];

		//10フレーム以外の処理
		if ($i != 10) {
			//スペアの場合は表示を記号にする
			if ($pin == 0 && $j == 2) {
				print "<td>／</td>";

			//ストライクの場合は表示を記号にする
			} elseif ($pin == 0 && $j == 1 ) {
				print "<td colspan=\"2\">×</td>";
				break;

			//その他の場合は倒したピン数を表示
			} else {
				print "<td>{$result[$i][$j]}</td>";
			}
		//10フレームのときの処理
		} else {
			//連続で10ピン倒したときはストライクの記号を表示する
			if ($pin == 0 && $flag == 1) {
				print "<td>×</td>";
				//10ピンをセットする
				$pin=10;
			//2投目か3投目でピンの数が0の場合はスペアの記号を表示する
			} elseif ($pin == 0 && ($j == 2 or $j == 3)) {
				print "<td>／</td>";
				$pin=10;
				$flag = 1;
			//初球でストライクの場合はストライクの記号を表示する
			} elseif ($pin == 0 && $j == 1 ) {
				print "<td>×</td>";
				//10ピンセットしてフラグを1にする。
				$pin=10;
				$flag = 1;
			//その他の場合は得点をそのまま表示する
			} else {
				print "<td>{$result[$i][$j]}</td>";
				$flag = 0;
			}
		
		}
	}
}
print "</tr>";

//フレームでの得点を表示する（10フレーム分
print "<tr>";
for ($i=1;$i<=10;$i++) {
	$col = ($i==10) ? 3 : 2;
	print "<td colspan=\"{$col}\">{$frame_cnt[$i]}</td>";

}
print "</tr>";

print <<< EOF
</table>
</body>
</html>
EOF;
/* HTMLで表示する部分 ここまで*/
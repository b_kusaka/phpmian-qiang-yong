	/* 自動で倒したピンを求めるロジック */
	//ボーリングの得点結果を格納する配列
	$result = array();
	$pin = 10;	//ピンの数を保持
	$flag = 0;	//10フレームの3投目判定用のフラグ
	
	//1ゲームから10ゲームまで繰り返す
	for ($i = 1; $i <= 10; $i++) {
		$end_flg = 0;	//終了を判断するためのフラグ
		for ($j = 1; $j <= 3; $j++) {
			if ($j == 1) {
				$nowpin = $pin;
			}
			//10ゲームの処理を分ける
			if ($i == 10) {
				$pin_out = rand(0,$nowpin);
				$result[$i][$j] = $pin_out;
				$nowpin = $nowpin - $pin_out;
				//10本倒していたら、10本セットして、3投目用のフラグを1にする。
				if ($nowpin == 0) {
					$nowpin = $pin;
					$flag = 1;
				}
				//2投内でストライクまたはスペアが出ていない場合は3投目はないので処理終了
				if($j == 2 && $flag == 0) {
					break;
				}
			} else {
				//10ゲーム以外は3投目はないので、繰り返し終了
				if ($j == 3) {
					break;
				}
				$pin_out = rand(0,$nowpin);
				$result[$i][$j] = $pin_out;
				$nowpin = $nowpin - $pin_out;
				//ストライクの判定
				if ($nowpin == 0) {
					break;
				}
			}
		}
	}
	/* 自動で倒したピンを求めるロジック ここまで */
print "<pre>";
print_r($result);

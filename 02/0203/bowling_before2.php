<?php
/* 自動で倒したピンを求めるロジック */
//ボーリングの得点結果を格納する配列
$result = array();
$pin = 10;	//ピンの数を保持
$flag = 0;	//10フレームの3投目判定用のフラグ

//1ゲームから10ゲームまで繰り返す
for ($i = 1; $i <= 10; $i++) {
	$end_flg = 0;	//終了を判断するためのフラグ
	for ($j = 1; $j <= 3; $j++) {
		if ($j == 1) {
			$nowpin = $pin;
		}
		//10ゲームの処理を分ける
		if ($i == 10) {
			$pin_out = rand(0,$nowpin);
			$result[$i][$j] = $pin_out;
			$nowpin = $nowpin - $pin_out;
			//10本倒していたら、10本セットして、3投目用のフラグを1にする。
			if ($nowpin == 0) {
				$nowpin = $pin;
				$flag = 1;
			}
			//2投内でストライクまたはスペアが出ていない場合は3投目はないので処理終了
			if($j == 2 && $flag == 0) {
				break;
			}
		} else {
			//10ゲーム以外は3投目はないので、繰り返し終了
			if ($j == 3) {
				break;
			}
			$pin_out = rand(0,$nowpin);
			$result[$i][$j] = $pin_out;
			$nowpin = $nowpin - $pin_out;
			//ストライクの判定
			if ($nowpin == 0) {
				break;
			}
		}
	}
}
/* 自動で倒したピンを求めるロジック ここまで */

/* 自動で倒したピンの得点計算をする部分 */
$sum = 0;	//合計得点
$frame_cnt = array();	//frameごとの得点
$strike_flg= array();

//1ゲームから10ゲームまで繰り返す
for($i=1;$i<=10;$i++) {
	$nowpin = 10;
	$cnt = 0;
	for($j=1;$j<=3;$j++) {
		//10ゲーム以外は3投目はないので、繰り返し終了
		if($j == 3 && $i != 10){break;}

		//値が空の場合は次の値へ
		if(!isset($result[$i][$j])){continue;}

		$nowpin = $nowpin-$result[$i][$j];
		$sum += $result[$i][$j];
		//ストライクの場合
		if($nowpin == 0 && $j == 1 && $i != 10){
			$cnt = 2;
		}elseif($nowpin == 0 && $j == 2 && $i != 10){
			$cnt = 1;
		}
		if($cnt >= 1) {
			for($k=$i+1;$k<=10;$k++) {
				for($l=1;$l<=3;$l++) {
					if(!isset($result[$k][$l])){continue;}
					if($l == 3 || $cnt <= 0){break;}
						$sum += $result[$k][$l];
						$cnt--;
				}
			}
			break;
		}
	}
	$frame_cnt[$i] = $sum;
}
/* 自動で倒したピンの得点計算をする部分 ここまで */
print "<pre>";
print_r($result);
print_r($frame_cnt);


?>
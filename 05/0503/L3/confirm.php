<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");

//初期化関数を呼び出す
init();

// 正しく入力されていないと確認画面へは遷移してくることができない
if (error_check($_SESSION)) {
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/input.php";
	header("Location: " . $url);
	exit;
}

//下記に２つのボタンのアクションを追加します


// Smartyを生成する
$smarty = new MySmarty();

// 性別リストと年齢リストと動物のリストの配列
$smarty->assign("sex_value",   getSexList());
$smarty->assign("age_value",   getAgeList());
$smarty->assign("animal_value",getAnimalList());

$smarty->display("confirm.tpl");

?>
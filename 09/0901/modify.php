<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");

//初期化関数を呼び出す
init();

//ログイン状態のチェックする関数を呼び出す
loginCheck();

//一覧からのanq_idを受け取るための処理
if (isset($_GET["id"])) {
	//データベースに接続する関数を呼び出す
	$db = db_connect();
	//PEARのDBライブラリにあるquoteSmartメソッドを使用して、SQLで使用してはいけない文字列をエスケープ
	$q_anq_id = $db->quoteSmart($_GET['id']);
	//quoteSmartメソッドは「'」を付加するので、「'」で囲まない
	$sql = "SELECT * FROM anq_t WHERE anq_id = {$q_anq_id}";

	//SQLで実行した結果を連想配列データで1レコード$rowに取得する
	$anq_data = $db->getRow($sql,DB_FETCHMODE_ASSOC);

	//データが空の場合はアンケート結果一覧へ遷移する
	if (!$anq_data) {
		$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/anq_result.php";
		header("Location: " . $url);
		exit;
	}

	//取得したアンケートデータをセッションへセットする
	$_SESSION["anq_id"] = $anq_data["anq_id"];
	$_SESSION["name"]   = isset($anq_data["name"])   ? $anq_data["name"] : "";
	$_SESSION["sex"]    = isset($anq_data["sex"])    ? $anq_data["sex"]  : "";
	$_SESSION["age"]    = isset($anq_data["age"])    ? $anq_data["age"]  : "";
	$_SESSION["animal"] = isset($anq_data["animal"]) ? explode(",", $anq_data["animal"]) : "";
	$_SESSION["comment"]= isset($anq_data["comment"])? $anq_data["comment"] : "";
}

// セッションにアンケートIDがセットされていない場合は不正遷移
if (!isset($_SESSION["anq_id"]) && !is_numeric($_SESSION["anq_id"])) {
		$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/anq_result.php";
		header("Location: " . $url);
		exit;
}

// キャンセルボタンがクリックされたとき
if (isset($_POST["cancel"])) {
	
	// 不要なセッションを削除
	$_SESSION["anq_id"] = "";
	$_SESSION["name"]   = "";
	$_SESSION["sex"]    = "";
	$_SESSION["age"]    = "";
	$_SESSION["animal"] = "";
	$_SESSION["comment"]= "";
	
	//アンケート結果一覧へ遷移する
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/anq_result.php";
	header("Location: " . $url);
	exit;
}

//エラーメッセージを受け取る変数の初期化
$error_list = array();

// 登録確認ボタンがクリックされたとき
if (isset($_POST["confirm"])) {
	// アンケートデータをセッションに格納する
	$_SESSION["name"]    = isset($_POST["name"])    ? $_POST["name"]    : "";
	$_SESSION["sex"]     = isset($_POST["sex"])     ? $_POST["sex"]     : "";
	$_SESSION["age"]     = isset($_POST["age"])     ? $_POST["age"]     : "";
	$_SESSION["animal"]  = isset($_POST["animal"])  ? $_POST["animal"]  : "";
	$_SESSION["comment"] = isset($_POST["comment"]) ? $_POST["comment"] : "";
	
	// エラーチェック関数の呼び出し、エラーメッセージを受け取る
	$error_list = error_check($_SESSION);

	//エラーがなければ、確認画面へ遷移する
	if (!$error_list) {
		$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/confirm.php";
		header("Location: " . $url);
		exit;
	}
}

//MySmartyクラスを生成する
$smarty = new MySmarty();

//性別リストと年齢リストと動物リストの配列をassignする
$smarty->assign("sex_value",getSexList());
$smarty->assign("age_value",getAgeList());
$smarty->assign("animal_value",getAnimalList());

//エラーメッセージリストをassignする
$smarty->assign("error_list",$error_list);

//入力フォームを表示する
$smarty->display("admin/modify.tpl");

?>
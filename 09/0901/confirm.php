<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");

//初期化関数を呼び出す
init();

//ログイン状態のチェック
loginCheck();

// 正しく入力されていないと確認画面へは遷移してくることができない
if (error_check($_SESSION)) {
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/anq_result.php";
	header("Location: " . $url);
	exit;
}

//下記に２つのボタンのアクションを追加します
	
// 修正ボタンをクリックされたときの処理
if (isset($_POST["modify"])) {
	// 入力フォームへ遷移する
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/modify.php";
	header("Location: " . $url);
	exit;
}

// 登録ボタンをクリックされたときの処理
if (isset($_POST["register"])) {
	// データベースに接続する関数の呼び出し
	$db = db_connect();

	// mysql_real_escape_string関数を使用せずに、データベースに依存しない
	// PEARのDBライブラリにあるquoteSmartメソッドを使用して、SQLで使用してはいけない文字列をエスケープします。
	$anq_id      = $db->quoteSmart($_SESSION["anq_id"]);
	$name_val    = $db->quoteSmart($_SESSION["name"]);
	$sex_val     = $db->quoteSmart($_SESSION["sex"]);
	$age_val     = $db->quoteSmart($_SESSION["age"]);
	$animal_val  = $db->quoteSmart(join(",",$_SESSION["animal"]));
	$comment_val = $db->quoteSmart($_SESSION["comment"]);
		
	// 不要なセッションを削除する
	$_SESSION['anq_id'] = "";
	$_SESSION['name']   = "";
	$_SESSION['sex']    = "";
	$_SESSION['age']    = "";
	$_SESSION['animal'] = "";
	$_SESSION['comment']= "";

	// データ修正前にIDの存在チェック
	$sql = "SELECT * FROM anq_t WHERE anq_id = {$anq_id}";
	$anq_data = $db->getRow($sql,DB_FETCHMODE_ASSOC);
	
	if (!$anq_data) {
		print "エラーが発生しました。修正しようとしたアンケートデータは存在しません。<br />";
		exit;
	} else {
		// データを修正するSQL文の作成と発行
		// エスケープした値には「'」が付加されるので、「'」で囲っているのをはずします。
		$sql = "UPDATE anq_t set name = {$name_val},sex = {$sex_val},age = {$age_val},animal = {$animal_val},comment = {$comment_val} WHERE anq_id = {$anq_id}";
		$res = $db->query($sql);
		// 登録処理が正常に行なえた場合は登録完了ページへ遷移する。
		if ( DB::isError($res) ) {
			print "エラーが発生しました。再度、アンケート結果一覧より修正ください。<br />";
			exit;
		}
	}

	// 完了画面を表示して終了
	$smarty = new MySmarty();
	$smarty->display("admin/complete.tpl");
	exit;
}

// Smartyを生成する
$smarty = new MySmarty();

// 性別リストと年齢リストと動物のリストの配列
$smarty->assign("sex_value",   getSexList());
$smarty->assign("age_value",   getAgeList());
$smarty->assign("animal_value",getAnimalList());

$smarty->display("admin/confirm.tpl");

?>
<?php

//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//初期化関数を呼び出す
init();

//ログイン状態をチェックする関数を呼び出す
loginCheck();

//性別と年代と動物のリストを変数に格納する
$sex_value    = getSexList();
$age_value    = getAgeList();
$animal_value = getAnimalList();

//データベースに接続
$db = db_connect();

//アンケートのデータをすべて取得（取得順はデータ登録日順
$sql = "SELECT * FROM anq_t WHERE del_flag != '1' ORDER BY create_datetime DESC";
$anq_list = $db->getAll($sql,DB_FETCHMODE_ASSOC);

//ここからCSV処理を追加します

?>
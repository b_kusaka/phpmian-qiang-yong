<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//ログインのチェックのためにセッションスタート
session_start();

//ログイン状態をチェックする関数を呼び出す
loginCheck();

//データベースに接続
$db = db_connect();

//JpGraphが格納されているフォルダーを保持
$jpgraph_dir = $_SERVER["DOCUMENT_ROOT"]. "/../libs/jpgraph-2.2/src/";

//JpGraphのライブラリを読み込む（棒Graph
require_once($jpgraph_dir . "jpgraph.php");
require_once($jpgraph_dir . "jpgraph_bar.php");
require_once($jpgraph_dir . "jpgraph_line.php");
require_once($jpgraph_dir . "jpgraph_pie.php");

//年代で集計をかけてカウントするSQLを実行
$sql = "SELECT age,COUNT(*) as num FROM anq_t WHERE del_flag != '1' GROUP BY age";
$anq_list = $db->getAll($sql,DB_FETCHMODE_ASSOC);

//YとXのデータを格納する変数
$datay=array();
$datax=array();

//年代を取得
$age_value = getAgeList();

//Xに表示するラベルとYに表示する人数をセット
foreach((array)$anq_list as $key => $value) {
	$datay[]=$value["num"];
	$datax[]=$age_value[$value["age"]];
}

//タイトルのセット
$title="年齢ごとのグラフ";

//XのタイトルとYのタイトルをセット
$xtitle="年代";
$ytitle="人数";

//グラフの表示サイズ（横
$width = "300";


//ここからグラフ設定追加









?>
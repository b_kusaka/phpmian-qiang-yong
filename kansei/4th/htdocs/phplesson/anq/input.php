<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");

//初期化関数を呼び出す
init();

// 登録確認ボタンがクリックされたとき
if (isset($_POST["confirm"])) {
	
	/*
	 * ボタンがクリックされた後の処理は第5章で行います。
	 
	// アンケートデータをセッションに格納する
	
	// エラーチェックと、エラーメッセージ出力は第5章で行います
	if (!$error_list) {
		// エラーがなければ、確認画面へ遷移するようにプログラミングする
	}
	*/
}

/* ここからSmartyの処理を追加 */

//MySmartyクラスを生成する
$smarty = new MySmarty();

//性別リストと年齢リストと動物リストの配列をassignする
$smarty->assign("sex_value",getSexList());
$smarty->assign("age_value",getAgeList());
$smarty->assign("animal_value",getAnimalList());

//入力フォームを表示する
$smarty->display("input.tpl");

?>
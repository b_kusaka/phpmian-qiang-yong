<?php
//Smartyクラスの呼び出し
require_once("../../../libs/MySmarty.class.php");

//変数・配列に値をセット
$scalar = "Hello Smarty!";
$sex["m"] = "men";
$sex["f"] = "woman";

//MySmartyインスタンス生成
$smarty = new MySmarty();

//テンプレート変数の割り当て
$smarty->assign("scalar", $scalar);
$smarty->assign("sex", $sex);

//テンプレートの表示
$smarty->display("no4/smarty1.tpl");

?>
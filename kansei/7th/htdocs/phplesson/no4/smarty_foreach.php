<?php
//Smartyクラスの呼び出し
require_once("../../../libs/Smarty.class.php");

//一部の都道府県の配列をセット
$pref[1]  = "北海道";
$pref[2]  = "青森県";
$pref[3]  = "岩手県";
$pref[4]  = "宮城県";
$pref[5]  = "秋田県";
$pref[6]  = "山形県";
$pref[13] = "東京都";
$pref[47] = "沖縄県";

//Smartyインスタンス生成
$smarty = new Smarty();

//テンプレート変数の割り当て
$smarty->assign("pref", $pref);

//チェックボックスのデフォルトチェックを配列で指定する
//ここでは山形(6)と東京(13)をデフォルトチェックにした例
$smarty->assign("checked", array("6","13"));

//テンプレートの表示
$smarty->display("smarty_foreach.tpl");

?>
<?php
//PEARのDBライブラリを読み込む
require_once("DB.php");

// データベースとの接続情報を記述
$dsn = "mysql://dbuser:pass@localhost/phplesson";

//データベースへ接続
$db =& DB::connect($dsn);
if (DB::isError($db)) {
	print "ConnectError!";
	exit;
}

//address_tテーブルへデータを更新する
$sql = "UPDATE address_t SET birthday='1980-05-22',address='東京都港区' WHERE number = '8'";

//DBのqueryメソッドを使用してSQL文を実行します
$res = $db->query($sql);

//何行のデータが反映したのかを取得するメソッドです
$num = $db->affectedRows();

//反映された数を表示する
print $num . "rows Update<br />";

//address_tテーブルのデータを削除する
$sql = "DELETE FROM address_t WHERE number = '5'";

//DBのqueryメソッドを使用してSQL文を実行します
$res = $db->query($sql);

//何行のデータが反映したのかを取得するメソッドです
$num = $db->affectedRows();

//反映された数を表示する
print $num . "rows Delete";

?>
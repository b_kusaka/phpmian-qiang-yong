<?php
//PEARのHTTP_Requestライブラリを読み込む
require_once("HTTP/Request.php");

//最初に取得するURLをセットして、HTTP_Requestライブラリを作成
$req = new HTTP_Request("http://www.yahoo.com/");

//yahoo.comの内容を取得する
if (!PEAR::isError($req->sendRequest())) {
     $response1 = $req->getResponseBody();
} else {
     $response1 = "";
}

//取得するphp.netのURLをセットする
$req->setURL("http://php.net");

//php.netの内容を取得する
if (!PEAR::isError($req->sendRequest())) {
     $response2 = $req->getResponseBody();
} else {
     $response2 = "";
}

//取得したYahoo.comとphp.netのページを表示する
print $response1;
print $response2;

?>
<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../libs/function.php");

//初期化関数を呼び出す
init();

// 正しく入力されていないと確認画面へは遷移してくることができない
if (error_check($_SESSION)) {
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/input.php";
	header("Location: " . $url);
	exit;
}

//下記に２つのボタンのアクションを追加します
	
// 修正ボタンをクリックされたときの処理
if (isset($_POST["modify"])) {
	// 入力フォームへ遷移する
	$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/input.php";
	header("Location: " . $url);
	exit;
}

// 登録ボタンをクリックされたときの処理
if (isset($_POST["register"])) {
	// データベースに接続する関数の呼び出し
	$db = db_connect();

	// mysql_real_escape_string関数を使用せずに、データベースに依存しない
	// PEARのDBライブラリにあるquoteSmartメソッドを使用して、SQLで使用してはいけない文字列をエスケープします。
	$name_val    = $db->quoteSmart($_SESSION["name"]);
	$sex_val     = $db->quoteSmart($_SESSION["sex"]);
	$age_val     = $db->quoteSmart($_SESSION["age"]);
	$animal_val  = $db->quoteSmart(join(",",$_SESSION["animal"]));
	$comment_val = $db->quoteSmart($_SESSION["comment"]);
		
	// 不要なセッションを削除する
	$_SESSION = array();

	// データを登録するSQL文の作成と発行(削除フラグは'0'で登録します
	// エスケープした値には「'」が付加されるので、「'」で囲っているのをはずします。
	$sql = "INSERT INTO anq_t(name,sex,age,animal,comment,del_flag,create_datetime) VALUES ({$name_val},{$sex_val},{$age_val},{$animal_val},{$comment_val},'0',now())";
	$res = $db->query($sql);

	// 登録処理が正常に行なかった場合はエラーメッセージを出力してプログラムを終了
	if ( DB::isError($res) ) {
		print "エラーが発生しました。再度、アンケートフォームよりご登録ください。<br />";
		exit;
	}

	// 完了画面を表示して終了
	$smarty = new MySmarty();
	$smarty->display("complete.tpl");
	exit;
}

// Smartyを生成する
$smarty = new MySmarty();

// 性別リストと年齢リストと動物のリストの配列
$smarty->assign("sex_value",   getSexList());
$smarty->assign("age_value",   getAgeList());
$smarty->assign("animal_value",getAnimalList());

$smarty->display("confirm.tpl");

?>
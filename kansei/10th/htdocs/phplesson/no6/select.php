<?php
//MySQLへ接続
$mysql_con = mysql_connect ("localhost","dbuser","pass");
if ($mysql_con == false) {
	print "DB_Connect Error!";
	exit;
}

//MySQLのデータベースを選択
$select_db = mysql_select_db ("phplesson",$mysql_con); 
if ($select_db == false) {
	print "DB_SELECT Error!";
	exit;
}

//address_tテーブルのデータを全て取得する。
$sql = "SELECT * FROM address_t";

//mysql_query関数にSELECT文を記述した関数と$mysql_conのMySQLの接続情報を引数に与えてクエリを実行する
$result = mysql_query ($sql,$mysql_con);

//テーブルの内容を出力する
print "<html>";
print "<body>";
print "<table border=1>";
print "<tr><td>番号</td><td>名前</td><td>メールアドレス</td><td>誕生日</td><td>住所</td><td>説明</td></tr>";

//mysql_fetch_array関数を使用して、$resultのSELECTの結果から、1行ずつデータ$rowに取得します
//whileの繰り返しは、データが無くなるまで繰り返します
while ($row = mysql_fetch_array ($result)) {
	print "<tr><td>" . $row["number"] . "</td>";
	print "<td>" . $row["name"] . "</td>";
	print "<td>" . $row["mail"] . "</td>";
	print "<td>" . $row["birthday"] . "</td>";
	print "<td>" . $row["address"] . "</td>";
	print "<td>" . $row["explanation"] . "</td></tr>";
}
print "</table>";
print "</body>";
print "</html>";
?>
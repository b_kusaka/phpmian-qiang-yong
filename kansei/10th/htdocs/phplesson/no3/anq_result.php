<?php
//セッションを開始する
session_start();

//ログインが成功した証拠である変数のチェックを行なう
if (!isset($_SESSION["login_name"])) {
	//変数に値がセットされていない場合は不正な処理とみなし、ブラウザをログイン画面へリダイレクトさせます
	header("Location: http://" . $_SERVER["HTTP_HOST"] . "/phplesson/no3/login.php");
	exit;
}
?>
この文字がブラウザに表示されたら、ログイン成功です。

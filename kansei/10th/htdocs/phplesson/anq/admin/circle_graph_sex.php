<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//ログインのチェックのためにセッションスタート
session_start();

//ログイン状態をチェックする関数を呼び出す
loginCheck();

//データベースに接続
$db = db_connect();

//JpGraphが格納されているフォルダーを保持
$jpgraph_dir = $_SERVER["DOCUMENT_ROOT"]. "/../libs/jpgraph-2.2/src/";

//JpGraphのライブラリを読み込む（棒Graph
require_once($jpgraph_dir . "jpgraph.php");
require_once($jpgraph_dir . "jpgraph_bar.php");
require_once($jpgraph_dir . "jpgraph_line.php");
require_once($jpgraph_dir . "jpgraph_pie.php");

//性別を取得
$sex_value = getSexList();

//性別で集計をかけてカウントするSQLを実行
$sql = "SELECT sex,COUNT(*) as num FROM anq_t WHERE del_flag != '1' GROUP BY sex";
$anq_list = $db->getAll($sql,DB_FETCHMODE_ASSOC);

//YとXのデータを格納する変数
$datay=array();
$datax=array();

//Xに表示するラベルとYに表示する人数をセット
foreach((array)$anq_list as $key => $value) {
	$datay[]=$value["num"];
	$datax[]=$sex_value[$value["sex"]];
}
//タイトルのセット
$title = "性別ごとのグラフ";

//XのタイトルとYのタイトルをセット
$xtitle="性別";
$ytitle="人数";

//グラフの表示サイズ（横
$width = "300";

//円Graph用のGraphオブジェクトを生成します。
$graph = new PieGraph($width,200,"auto");

//Graphに影をセットします。
$graph->SetShadow();

//Graphのフォントとタイトルをセットします
$graph->title->Set($title);
$graph->title->SetFont(FF_MINCHO, FS_NORMAL, 12);

//レジェンド（説明）のフォントをセットします
$graph->legend->SetFont(FF_MINCHO, FS_NORMAL);

//円グラフのオブジェクトを生成します。（Y軸のデータを挿入する
$p1 = new PiePlot($datay);

//円Graphのテーマをセット
$p1->SetTheme("earth");

//円の中心の位置を指定します
$p1->SetCenter(0.4);

//レジェンド（説明）をセットします
$p1->SetLegends($datax);

//Graphに円Graphを追加します。
$graph->Add($p1);

// Graphを表示します
$graph->Stroke();

?>
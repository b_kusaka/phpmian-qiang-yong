<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//ログインのチェックのためにセッションスタート
session_start();

//ログイン状態をチェックする関数を呼び出す
loginCheck();

//データベースに接続
$db = db_connect();

//JpGraphが格納されているフォルダーを保持
$jpgraph_dir = $_SERVER["DOCUMENT_ROOT"]. "/../libs/jpgraph-2.2/src/";

//JpGraphのライブラリを読み込む（棒Graph
require_once($jpgraph_dir . "jpgraph.php");
require_once($jpgraph_dir . "jpgraph_bar.php");
require_once($jpgraph_dir . "jpgraph_line.php");
require_once($jpgraph_dir . "jpgraph_pie.php");

//動物の数値をカウントするSQLを実行
$sql = "SELECT animal FROM anq_t WHERE del_flag != '1'";
$anq_list = $db->getAll($sql,DB_FETCHMODE_ASSOC);

//YとXのデータを格納する変数
$datax = array();
$datay = array();

//動物を取得
$animal_value = getAnimalList();

foreach ($animal_value as $key => $value) {
	$datax[$key - 1] = $value;
	$datay[$key - 1] = 0;
}

//好きな動物の「,」区切りのデータを配列データにする
foreach ((array)$anq_list as $key => $value ) {
	$tmp_animal = explode(",",$value["animal"]);
	foreach ((array)$tmp_animal as $num => $animalno ) {
		$animalno = $animalno - 1;
		$datay[$animalno] = $datay[$animalno] + 1;
	}
}

//タイトルのセット
$title="動物ごとのグラフ";

//XのタイトルとYのタイトルをセット
$xtitle="好きな動物";
$ytitle="人数";

//グラフの表示サイズ（横
$width = "500";

//円Graph用のGraphオブジェクトを生成します。
$graph = new PieGraph($width,200,"auto");

//Graphに影をセットします。
$graph->SetShadow();

//Graphのフォントとタイトルをセットします
$graph->title->Set($title);
$graph->title->SetFont(FF_MINCHO, FS_NORMAL, 12);

//レジェンド（説明）のフォントをセットします
$graph->legend->SetFont(FF_MINCHO, FS_NORMAL);

//円グラフのオブジェクトを生成します。（Y軸のデータを挿入する
$p1 = new PiePlot($datay);

//円Graphのテーマをセット
$p1->SetTheme("earth");

//円の中心の位置を指定します
$p1->SetCenter(0.4);

//レジェンド（説明）をセットします
$p1->SetLegends($datax);

//Graphに円Graphを追加します。
$graph->Add($p1);

// Graphを表示します
$graph->Stroke();

?>
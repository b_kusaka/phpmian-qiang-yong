<?php
//現在のinclude_pathにFPDFの格納場所をinclude_pathに追記でセットします。
$path = 'C:\xampp\libs\fpdf';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//初期化関数を呼び出す
init();

//ログイン状態のチェック
loginCheck();

//PDFライブラリを読み込む
require_once('mbfpdf.php');

//データベースに接続
$db = db_connect();

//送られたアンケートIDを調べる。
if (isset($_GET["anq_id"]) && is_numeric($_GET["anq_id"])) {
	$sql = "SELECT * FROM anq_t WHERE anq_id = " . $db->quoteSmart($_GET["anq_id"]);
	$anq_data = $db->getRow($sql,DB_FETCHMODE_ASSOC);

	//データが取得できなかったら処理を終了する
	if ($anq_data["anq_id"] == "") {
		exit();
	}
//IDが正しい形式でない場合は処理を終了する
} else {
	exit;
}

//性別と年代と動物のリストを変数に格納する
$sex_value    = getSexList();
$age_value    = getAgeList();
$animal_value = getAnimalList();


//UTF-8からSJISに文字コードを変換する（FPDFがSJISで表示するため。
$name     = mb_convert_encoding($anq_data["name"], "SJIS","UTF-8");
$sex      = mb_convert_encoding($sex_value[$anq_data["sex"]], "SJIS","UTF-8");
$age      = mb_convert_encoding($age_value[$anq_data["age"]], "SJIS","UTF-8");
$comment  = mb_convert_encoding($anq_data["comment"], "SJIS","UTF-8");
$datetime = mb_convert_encoding($anq_data["create_datetime"], "SJIS","UTF-8");

//好きな動物の「,」区切りのデータを配列データに変換して、配列データを展開して動物名をつなげる処理
$animal   = "";
$tmp_animal = explode(",",$anq_data["animal"]);
foreach ((array)$tmp_animal as $tmpkey => $animalno) {
	$animal  .= mb_convert_encoding($animal_value[$animalno], "SJIS","UTF-8") . " ";
}

//MBPDFクラスを生成する。
$pdf=new MBFPDF();

//使用するフォントを追加します。
$pdf->AddMBFont(GOTHIC ,'SJIS');

//ドキュメントを生成
$pdf->Open();

//左のマージンを4cmセットする
$pdf->SetLeftMargin(40);

//新規ページ追加
$pdf->AddPage();

//使用するフォントをセットします。
$pdf->SetFont(GOTHIC,'B',20);

//取得したデータをCellで書き込む
$pdf->Cell(50,10,mb_convert_encoding("アンケート内容", "SJIS","UTF-8"),0,1,'C'); 

$pdf->Cell(40,10,mb_convert_encoding("名前", "SJIS","UTF-8"),1,0,'C'); 
$pdf->Cell(110,10,$name,1,1,'C'); 

$pdf->Cell(40,10,mb_convert_encoding("性別", "SJIS","UTF-8"),1,0,'C'); 
$pdf->Cell(110,10,$sex,1,1,'C'); 

$pdf->Cell(40,10,mb_convert_encoding("年代", "SJIS","UTF-8"),1,0,'C'); 
$pdf->Cell(110,10,$age,1,1,'C'); 

$pdf->Cell(40,10,mb_convert_encoding("投稿日", "SJIS","UTF-8"),1,0,'C'); 
$pdf->Cell(110,10,$datetime,1,1,'C'); 

//セルからはみ出す場合があるので、MultiCellメソッドを使用して表示する
$pdf->Cell(40,10,mb_convert_encoding("好きな動物", "SJIS","UTF-8"),1,0,'C'); 
$pdf->MultiCell(110,10,$animal,1,1,'L'); 

//セル内に改行がある場合があるので、MultiCellメソッドを使用して表示する
$pdf->Cell(40,10,mb_convert_encoding("コメント", "SJIS","UTF-8"),1,0,'C'); 
$pdf->MultiCell(110,10,$comment,1,1,'L'); 

$pdf->Cell(150,5,"",0,1,'C'); 

//PDFをダウンロードさせる形式で出力します。
$pdf->Output("anq_data.pdf","D");
?>
<?php 
//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//初期化関数を呼び出す
init();

//ログイン状態をチェックする関数を呼び出す
loginCheck();

//Smartyを生成
$smarty = new MySmarty(); 
$smarty->display("admin/anq_graph.tpl"); 
?>
<?php
//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//初期化関数を呼び出す
init();

//セッションデータをすべて削除（クリア）する
$_SESSION = array();

//ログイン画面（login.php）へ遷移して処理を終了させる
$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/login.php";
header("Location: " . $url);
exit;
?>

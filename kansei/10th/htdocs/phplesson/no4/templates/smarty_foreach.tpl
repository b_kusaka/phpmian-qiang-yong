■住所
<select name="pref">
  {* 都道府県の配列がなくなるまで繰り返す *}
  {foreach from=$pref item=pref_name key=num}
    {* keyをオプションのvalue値にして、配列の内容をオプションのテキストとして表示する *}
    <option value="{$num}">{$pref_name}</option>
  {* 都道府県の配列が空の場合の処理 *}
  {foreachelse}
    <option value="">選択できません</option>
  {/foreach}
</select>
<hr>
■cycle関数の使用例<br>
<table border="1">
<tr>
  <th>都道府県名</th>
</tr>
{foreach from=$pref item=pref_name key=num}
<tr bgcolor="{cycle values="#CB21DE,#35D0B5"}"> {* 最初に指定した値から交互に繰り返します *}
  <td>{$pref_name}</td>
</tr>
{/foreach}
</table>
<hr>
■option要素自動生成例<br>
自動生成１
<select name="pref">
  <option value="">選択してください</option>
  {html_options options=$pref} {* option要素のみを生成する *}
</select>

<br />

自動生成２
{html_options name=pref options=$pref selected=13} {* selectタグとoption要素の両方を生成する *}
<hr>
■ラジオボタン自動生成例<br>
住所１（ラジオボタン
{html_radios name='pref' options=$pref selected=13} {* ラジオボタンを一列に生成する *}

<br />

住所２（ラジオボタン<br />
{html_radios name='pref' options=$pref selected=13 separator='<br />'} {* ラジオボタンを要素ごとに改行して生成する *}
<hr>
■checkbox要素自動生成例<br>

住所<br />
{html_checkboxes name='pref' options=$pref selected=$checked separator='<br />'}


<?php
/* PEARのHTTP_Requestライブラリを読み込む */
require_once("HTTP/Request.php");

//ページのチェックをするURLリストを配列でセットする
$urls = array(
	"http://www.phppro.jp/",
	"http://example.com/thisdoesnotexist.html",
	"http://www.google.com"
	);

//HTTP_Requestライブラリを作成
$req = new HTTP_Request();

//URLリストがなくなるまでチェックする
foreach ($urls as $url) {

	//調べるURLをセットする
	$req->setURL($url);

	//リクエストを送る
	$req->sendRequest();

	//調べたURLを出力する
	print  "URL=" . $url . "  ";
	
	//リクエストに対するサーバーから送られてくるレスポンスコードを取得する
	//代表的なコード
	//404 Not Found（ページが存在しない場合に返すコード
	//403 Forbidden（ページを見つけられない場合に返すコード
	//200 OK（リクエストは正常に処理された
	
	$code = $req->getResponseCode();
	switch ($code) {
		case 404:
		print "ページが見つかりません。<br>";
		break;
		case 200:
		print "ページは存在します。<br>";
		break;
		default:
		print "その他のレスポンス番号(" . $code . ")を返しました。<br>";
		break;
		
	}
}
?>
<?php

//libsフォルダにある共通関数を読み込む
require_once("../../../../libs/function.php"); 

//初期化関数を呼び出す
init();

// Smartyを生成する
$smarty = new MySmarty(); 

//ログインボタンがクリックされた場合の処理
if (isset($_POST["login"])) { 

	//ログインIDとパスワードが入力されているかチェックする
	if ($_POST["loginid"] === "" || $_POST["password"] === "") { 
		$error_message = "ログインIDとパスワードを入力してください。"; 

	//ログインIDとパスワードが入力されていたら処理する
	} else {
		//ログイン関数を呼び出し、trueが返ってきたらログイン認証成功
		if (Authenticator($_POST["loginid"] , $_POST["password"])) { 
			//セッションにログインIDを保存する
			$_SESSION["login_name"] = $_POST["loginid"]; 

			//管理画面トップ（index.php）へ遷移して処理を終了させる
			$url = "http://" . $_SERVER["HTTP_HOST"] . dirname($_SERVER["SCRIPT_NAME"]) . "/index.php";
			header("Location: " . $url);
			exit;

		//ログイン認証失敗の場合はエラーメッセージをセットします
		} else { 
			$error_message = "ログインに失敗しました。";
		}
	}
	//入力したログインIDとエラーメッセージをアサインする
	$smarty->assign("loginid", $_POST["loginid"]); 
	$smarty->assign("error_message", $error_message); 
}

//テンプレートファイルを表示する（管理画面のテンプレートは「admin/」に格納されるので注意する
$smarty->display("admin/login.tpl"); 
?>

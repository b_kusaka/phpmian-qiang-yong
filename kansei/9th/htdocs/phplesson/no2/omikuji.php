<?php
	//「おみくじを引く」ボタンがクリックされたときに送られる変数があった場合の処理
	if(isset($_POST["start"])) {

		//占いデータを配列に格納する。
		$uranai_data[] = "大吉です。\nいい日になりそうです。";
		$uranai_data[] = "中吉です。\nいつもよりいいことが起こりそうです。";
		$uranai_data[] = "吉です。よくもなく悪くもない日です。";
		$uranai_data[] = "末吉です。\n運気が少し下がり目です。気をつけましょう。";
		$uranai_data[] = "凶です。\n今日は控えめな行動をとりましょう。";
		
		$max = count($uranai_data) - 1;
		$result = rand(0,$max);
		$message = "あなたの運勢は・・・" . $uranai_data[$result];
	}else{
		$message = "あなたの運勢をチェックしましょう。";
	}
?>
<html>
<head>
<title>おみくじプログラム</title>
</head>
<body>
<h2>今日のおみくじ</h2>
<h4>「おみくじを引く」ボタンをクリックすると結果が表示されるよ。</h4>
<form method="post">
<textarea name="result" rows="5" cols="40"><?php print $message;?></textarea>
<br>
<input type="submit" name="start" value="おみくじを引く">
</form>
</body>
</html>
<?php
$global_variable = 1;

print "ローカル変数(1):" . $global_variable . "<br />";
print "スーパーグローバル変数(1):" . $_GET["super_variable"] . "<br />";
my_function(); // my_function関数を呼び出す

function my_function() {
	print "ローカル変数(2):" . $global_variable . "<br />";
	print "スーパーグローバル変数(2):" . $_GET["super_variable"] . "<br />";

	global $global_variable; // $global_variable変数をグローバル化する
	print "グローバル化したローカル変数(3):" . $global_variable . "<br />";
}

?>
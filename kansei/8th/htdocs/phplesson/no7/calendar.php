<?php 
/* PEARのCalendarライブラリを使用してカレンダーを表示 */
?>
<html>
<head>
<title>カレンダー表示</title>
</head>
<body>
<h2>カレンダー</h2>
<?php

//PEARのCalendarライブラリを読み込む */
require_once("Calendar/Month/Weekdays.php");

//Calendarライブラリを作成する
$calMonth = new Calendar_Month_Weekdays(2007, 1, 0);

//カレンダーの子オブジェクトを作成
$calMonth->build();

//カレンダーを表示する
print "<h4>" . $calMonth->thisYear() . "年" . $calMonth->thisMonth() . "月のカレンダー</h4>";
print "<table border=1><thead><tr>";
print "<th>日</th><th>月</th><th>火</th><th>水</th><th>木</th><th>金</th><th>土</th>";
print "</tr></thead><tbody>";

//表示する月の最終日まで繰り返す
while ($day = $calMonth->fetch()) {
	if ($day->isFirst()) {
		print "<tr>";
	}
	if ($day->isEmpty()) {
		print "<td>&nbsp;</td>";
	} else {
		print "<td>".$day->thisDay()."</td>";
	}
	if ($day->isLast()) {
		print "</tr>";
	}
}
print "</tbody></table>";

?>
</body>
</html>
<?php

// プロフィール配列を定義する
$profile["name"]  = "海原才人";
$profile["pref"]  = "東京都";
$profile["sex"]   = "男";
$profile["email"] = "saity@asial.co.jp";

// プロフィールをテーブルで表示する
print "<table border=1>";

// 配列のキー名を$key 値を$valueに受け取る
foreach($profile as $key => $value) {
  // キーと値をそれぞれテーブルの要素として出力する
  print "<tr><td>{$key}</td><td>{$value}</td></tr>";
}
print "</table>";
?>

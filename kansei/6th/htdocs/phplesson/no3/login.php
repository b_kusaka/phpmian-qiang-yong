<?php
//セッションを開始する
session_start();

//エラーメッセージを格納する変数を初期化する
$error_message = "";

//ログインボタンを押されたか判定
//初めてのアクセスのときは認証は行なわず、エラーメッセージは出ないようプログラミングします
if (isset($_POST["login"])) {
	if ($_POST["member_name"] == "php" && $_POST["password"] == "password") {
		//ログイン成功
		$_SESSION["login_name"] = $_POST["member_name"];		//ログインが成功した証拠をセッションに保持する
		
		//管理者専用画面へブラウザをリダイレクトする
		header("Location: http://" . $_SERVER["HTTP_HOST"] . "/phplesson/no3/anq_result.php");
		exit;
	}
	$error_message = "メンバー名かパスワードが間違えています。";
}

?>
<html>
<body>
<?php
	if ($error_message) {
		print '<font color="red">' . $error_message . "</font>";
	}
?>
<form action="login.php" method="post">
	メンバー名：<input type="text" name="member_name" value=""><br />
	パスワード：<input type="password" name="password" value=""><br />
	<input type="submit" name="login" value="ログイン">
</form>
</body>
</html>

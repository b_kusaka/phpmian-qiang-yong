<?php

// ルートディレクトリを変数にセットする
define("ROOT_DIR", $_SERVER['DOCUMENT_ROOT']."/..");

// Smartyクラスを呼び出す
require_once("Smarty.class.php");

// Smartyクラスを継承したMySmartyクラスを作成する
class MySmarty extends Smarty {
	
	//コンストラクタ（コンストラクタとは、オブジェクトを生成する際に呼び出されて内容の初期化などを行なうメソッドです。
	function MySmarty () {
		
		// テンプレートディレクトリの上書き
		$this->template_dir   = ROOT_DIR . "/templates";
		
		// コンパイルディレクトリの上書き
		$this->compile_dir    = ROOT_DIR . "/templates_c";
		
		// 左右デリミタの上書き
		$this->left_delimiter  = "{{";   // デミリタとは、Smartyの変数や関数などを区切る記号です。
		$this->right_delimiter = "}}";   // デフォルトのデミリタだと開始区切りが「{」で終了区切りが「}」で区切るとSmartyの処理として認識される設定になっています。
		
		                                             // デフォルトで全ての変数にescapeをかける
		$this->default_modifiers = array('escape');  // Smartyのテンプレート内で使用するすべての変数にきまった処理を実行します。
		                                             // ここでは、「escape」という処理を行う設定にします。
		
		// Smartyクラスのコンストラクタの呼び出し
		$this->Smarty();
	}
}
?>
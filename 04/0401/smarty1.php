<?php
//Smartyクラスの呼び出し
require_once("../../../libs/Smarty.class.php");

//変数・配列に値をセット
$scalar = "Hello Smarty!";
$sex["m"] = "men";
$sex["f"] = "woman";

//Smartyインスタンス生成
$smarty = new Smarty();

//テンプレート変数の割り当て
$smarty->assign("scalar", $scalar);
$smarty->assign("sex", $sex);

//テンプレートの表示
$smarty->display("smarty1.tpl");

?>
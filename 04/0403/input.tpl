<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/base.css" rel="stylesheet" type="text/css" />
<title>アンケートフォーム</title>
</head>
<body>
<div class="sub-title">
アンケートフォーム
</div>
<div>
<font color="#FF0000">エラーメッセージはここに出力</font>
</div>
<form action="input.php" method="post">
<table class="editform">
<tr>
<th>名前</th>
<td><input type="text" name="name" value=""></td>
</tr>
<tr>
<th>性別</th>
<td>
<label for="sex_1"><input type="radio" name="sex" value="1" id="sex_1" />男性</label><br />
<label for="sex_2"><input type="radio" name="sex" value="2" id="sex_2" />女性</label><br />
</td>
</tr>
<tr>
<th>年代</th>
<td>
<select name="age">
<option value="">選択してください</option>
<option label="10代" value="1">10代</option>
<option label="20代" value="2">20代</option>
<option label="30代" value="3">30代</option>
<option label="40代" value="4">40代</option>
<option label="50代" value="5">50代</option>
<option label="60代以上" value="6">60代以上</option>
</select>
</td>
</tr>
<tr>
<th>コメント</th>
<td><textarea rows="10" cols="40" name="comment"></textarea></td>
</tr>
<tr>
<td colspan="2"><input type="submit" name="confirm" value="登録確認"></td>
</tr>
</table>
</form>
</body>
</html>

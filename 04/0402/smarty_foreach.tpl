■住所
<select name="pref">
  {* 都道府県の配列がなくなるまで繰り返す *}
  {foreach from=$pref item=pref_name key=num}
    {* keyをオプションのvalue値にして、配列の内容をオプションのテキストとして表示する *}
    <option value="{$num}">{$pref_name}</option>
  {* 都道府県の配列が空の場合の処理 *}
  {foreachelse}
    <option value="">選択できません</option>
  {/foreach}
</select>
